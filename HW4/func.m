function [func,func_i] = func(g,b_0,u_0,s_b,s_u,s_bu,D,M)

func_t = nan(D.N,1);
m_s = [b_0; u_0];
S =  [s_b, s_bu;
      s_bu, s_u];

[b,u] = qnwnorm([M M],mu,S);
for k=1:D.N
    x = D.X(:,k);
    y = D.Y(:,k);
    z = D.Z(:,k);
    
    func_t(k) = u'*indiv_lhd(D.X(:,k),D.Y(:,k),D.Z(:,k),...
                               b(:,1), g, b(:,2));
end

func_i = func_t;
func = -sum(log(func_i));


    function L_i = indiv_lhd(x,y,z,b,g,u)
        F = @(e) (1+exp(-e)).^(-1);
        arg = kron(b',x) + kron(g*ones(size(b')),z) + ...
              kron(u',ones(size(x)));
        L_i = prod(F(arg).^(kron(ones(size(b')), y)).* ...
                   (1-F(arg)).^(1 - kron(ones(size(b')),y)))';
    end
end