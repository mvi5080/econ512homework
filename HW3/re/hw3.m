clc
clear
load('hw3.mat')

%%%%%%%%%%%%%%%%%%%%% Question 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b_0= zeros(6,1);



%%%% FMINUNC without a derivative supplied
Opt_1 = optimoptions('fminunc','Display','iter','GradObj','off'  , 'Algorithm','quasi-newton','HessUpdate','bfgs');
b_wod = fminunc(@(x) log_lhd(X,y,x), b_0, Opt_1);
b_wod

%%%% FMINUNC with a derivative supplied
Opt_2=optimoptions('fminunc','Display','iter','GradObj','on' ,'Algorithm','quasi-newton','HessUpdate','bfgs');
b_wd = fminunc (@(x) log_lhd(X,y,x),b_0,Opt_2);
b_wd
%%%% Nelder Mead

optset ('neldmead','maxit',2000);
optset ('neldmeam','showiters',1);
beta_nm = neldmead (@(x)-log_lhd(X,y,x),b_0);
beta_nm
%%%% BHHH
tol = 1e-8;
b_BHHH = BHHH(X,y,b_0,tol); 
b_BHHH

%%%%%%%%%%%%%%%%%%%%% Question 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ei = eig(Hess(X, b_0))
es = eig(Hess(X, b_BHHH))
%%%%%%%%%%%%%%%%%%%%% Question 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Opt_3 = optimoptions('lsqnonlin','Display','iter');
b_nlls = lsqnonlin(@(beta) y-exp(X*beta), b_0,[],[],Opt_3);
b_nlls

diary











