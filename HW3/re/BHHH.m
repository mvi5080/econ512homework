function bc = BHHH(X,y,b_0,tol)

E = 10^10;
bi = b_0;

while E>=tol
   bt = BHHH_update(X,y,bi);
   E = max(abs(bt-bi));
   bi = bt;
end

bc=bi;
end