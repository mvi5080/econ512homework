function ihess = Hess(X,beta_0)

[n,k] = size(X);
hess_temp = zeros(k,k);

for i=1:n
    hess_temp = Hess_temp(X(i,:),beta_0) + hess_temp;
end

ihess = hess_temp;

    function H_temp = Hess_temp(x_loc,beta)
        H_temp = exp(x_loc*beta)*(x_loc')*(x_loc);
    end
end