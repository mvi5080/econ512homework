function bn = BHHH_update(X,y,beta)

[n,k] = size(X);
grds = nan(k,n);

for i=1:n
    grds(:,i) = grad(X(i,:),y(i),beta);   
end

G = sum(grds')';
J = zeros(k);

for i = 1:n
   J = grds(:,i)*grds(:,i)' + J; 
end

bn = beta + J\G;
    
    function g = grad(x_loc,y_loc,beta)
        g = -exp(x_loc*beta)*x_loc' + y_loc*x_loc';
    end
end