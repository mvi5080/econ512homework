function [ln, dln] = log_lhd(X,y,b_0)

[n,k]=size(X);
grds=nan(k,n);

ln = -sum(-exp(X*b_0) + y.*(X*b_0) - log(factorial(y)));

for i=1:n
    grds(:,i) = gra(X(i,:),y(i),b_0); 
end

dln = -sum(grds')';

    function g = gra(x_loc,y_loc,beta)
        g = -exp(x_loc*beta)*x_loc' + y_loc*x_loc'; 
    end
end