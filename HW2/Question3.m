function [X,Y] = Question3(vA,vB,vC,p)

options = optimset('Display','off');

qA = @(pA) exp(vA-pA)/(1+exp(vA-pA)+exp(vB-p(2))+exp(vC-p(3)));

funA=@(pA) qA(pA)-pA*qA(pA)*(1-qA(pA));

root1 = fsolve(funA,p(1),options); 
% Comment from TA: you were supposed to do just one oteration here to
% reduce the time taken for algorithm to run. it is not secant method, but
% you got the basic idea right. I don't know if you solving equation at
% each iteration is a cause of problem because proper Gauss Sidel actually
% converges for all initial guesses

qB = @(pB) exp(vB-pB)/(1+exp(vA-root1)+exp(vB-pB)+exp(vC-p(3)));

funB=@(pB)qB(pB)-pB*qB(pB)*(1-qB(pB));

root2=fsolve(funB,p(2),options);

qC = @(pC) exp(vC-pC)/(1+exp(vA-root1)+exp(vB-root2)+exp(vC-pC));

funC=@(pC)qC(pC)-pC*qC(pC)*(1-qC(pC));

root3=fsolve(funC,p(3),options);


X=[root1;
   root2;
   root3];

Y=abs(p-X); 
end