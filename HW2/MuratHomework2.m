%% ECON512 HOMEWORK2
% Murat Olus Inan

close all;
clear;
clc;


%-----------------------Q1-----------------------


[qA qB qC q0]= Question1(1,1,1,-1,-1,-1)  

%-----------------------Q2-----------------------
% Comments from TA: you are using singular FOC. Take a look at answer key
% definition of FOC


V       = [-1 -1 -1];  
pstar = @(p) Question2(V(1),V(2),V(3),p);  

%(a)
theguess= [1;1;1]
tol=1e-8;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(pstar,theguess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(b)
theguess= [0;0;0]
tol=1e-8;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(pstar,theguess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(c)
theguess= [0;1;2]
tol=1e-8;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(pstar,theguess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(d)
theguess= [3;2;1]
tol=1e-8;                      

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(pstar,theguess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)


%-----------------------Q3-----------------------

% Comment from TA: you were supposed to run this method on 4 different
% initial guesses
p0 = theguess;
dif = 1;
i = 1;
tic;
while abs(dif)>=tol
   [x,y] = Question3(-1,-1,-1,p0);
   p0 = x;
   dif = max(y);
   i = i+1;
end
time_gauss_seidel=toc;
sol_gauss_seidel = p0'
disp('Gauss-Seidel runtime')
disp(time_gauss_seidel)



%-----------------------Q4-----------------------


p0 = theguess;
dif = 1;
i = 1;
tic;
while abs(dif)>=tol
   x = Question4(-1,-1,-1,p0);
   dif = max(abs(x-p0));
   p0 = x;
   i = i+1;
end
time_iterate=toc;
sol_iterate=p0'
disp('Iteration method runtime')
disp(time_iterate)



%-----------------------Q5-----------------------


vC        = [-4,-3,-2,-1,0,1];
theguess = [0.8;1.1;1.05];
nl         = length(vC);
pA         = zeros(nl,1);
pC         = zeros(nl,1);
for i = 1:nl
    pfun  = @(p) Question2(-1,-1,vC(i),p);     
    thesol      = broyden(pfun,theguess);
    
    pA(i)         = thesol(1);
    pB(i)         = thesol(3);
end

figure
plot(vC,pA,vC,pB);
legend('pA','pC')

