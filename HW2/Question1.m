function [qA, qB, qC, q0]= q1(pA, pB, pC, vA, vB, vC)

qA = exp(vA-pA)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC)); 

qB = exp(vB-pB)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC));  

qC = exp(vC-pC)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC)); 

q0 = 1/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC));            

end