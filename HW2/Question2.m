function X = q2(vA, vB, vC, p)

qA = exp(vA-p(1))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3))); 

qB = exp(vB-p(2))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3))); 

qC = exp(vC-p(3))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3))); 

X = [qA-p(1)*qA*(1-qA); qB-p(2)*qB*(1-qB); qC-p(3)*qC*(1-qC)];                
end