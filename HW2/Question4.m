function X = Question4(vA,vB,vC,p)

qA = exp(vA-p(1))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3)));
qB = exp(vB-p(2))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3)));
qC = exp(vC-p(3))/(1+exp(vA-p(1))+exp(vB-p(2))+exp(vC-p(3)));

X=[inv(1-qA); inv(1-qB); inv(1-qC)];

qA2 = exp(vA-X(1))/(1+exp(vA-X(1))+exp(vB-X(2))+exp(vC-X(3)));
qB2 = exp(vB-X(2))/(1+exp(vA-X(1))+exp(vB-X(2))+exp(vC-X(3)));
qC2 = exp(vC-X(3))/(1+exp(vA-X(1))+exp(vB-X(2))+exp(vC-X(3)));

end